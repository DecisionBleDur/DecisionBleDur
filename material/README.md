# Folder material

Inside this folder is stored the material available for the Hackathon

## mod_swheat.sti

The **mod_swheat.sti** is the dataset provided in order to provide input datas coming from the STICS model.
In order to be usable it has been renamed to **mod_swheat.csv**.
And by the way the dos2unix command has been applied.