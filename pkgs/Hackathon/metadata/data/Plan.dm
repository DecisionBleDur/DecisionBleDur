<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE vle_project_metadata>
<vle_project_metadata version="1.x" author="meto">
 <dataPlugin package="vle.discrete-time.decision" name="Plan Activities"/>
 <dataModel outputsType="discrete-time" package="Hackathon" conf="Plan"/>
 <definition>
  <activities>
   <activity timeLag="1" deadline="1" value="-73" y="-86" minstart="+0-10-25" x="-283" name="SEMIS" maxfinish="+1-1-15" maxiter="1">
    <outputParams>
     
    
    
     <outputParam value="variete" name="sowing" type="Variable"/>
    </outputParams>
    <rulesAssigment>
     
    
    <rule name="portanceSol"/>
    </rulesAssigment>
   </activity>
  </activities>
  <parameters>
   
  
  
  <parameter value="15" name="ssp_semis" type="Par"/>
  </parameters>
  <predicates>
   
  <predicate operator="&lt;" leftValue="sp_semis" leftType="Var" rightValue="ssp_semis" name="portanceSol" rightType="Par"/>
  </predicates>
  <rules>
   
  <rule name="portanceSol">
    <predicate name="portanceSol"/>
   </rule>
  </rules>
  <precedences/>
 </definition>
 <configuration>
  <dynamic library="agentDTG" package="vle.discrete-time.decision" name="dynagentDTG"/>
  <observable name="obsPlan">
   <port name="KnowledgeBase"/>
   <port name="AchievedPlan"/>
   <port name="Activities"/>
   <port name="Activity_SEMIS"/>
   <port name="Activity(state)_SEMIS"/>
   <port name="Activity(ressources)_SEMIS"/>
   
  
  
  <port name="sp_semis"/>
  
   <port name="sowing"/>
  <port name="variete"/>
  </observable>
  <condition name="condPlan">
   <port name="dyn_allow">
    <boolean>1</boolean>
   </port>
   <port name="dyn_denys">
    <set/>
   </port>
   <port name="autoAck">
    <boolean>1</boolean>
   </port>
   <port name="PlansLocation">
    <string>Hackathon</string>
   </port>
   <port name="Rotation">
    <map>
     <key name="">
      <set>
       <integer>2147483647</integer>
       <set>
        <integer>1</integer>
        <string>Plan</string>
       </set>
      </set>
     </key>
    </map>
   </port>
  <port name="aDTGParameters">
    <map>
     <key name="ssp_semis">
      <double>15</double>
     </key>
    </map>
   </port>
  </condition>
  <in>
   
  <port name="sp_semis"/>
  <port name="variete"/>
  </in>
  <out>
   
  
  
   <port name="sowing"/>
  </out>
 </configuration>
</vle_project_metadata>
