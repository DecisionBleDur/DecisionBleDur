# variete.vpz

this note explane how to build the simulator.

+ copy the portanceSol.vpz simulator
+ renaming it to variete.vpz
+ adding an atomicModel
+ renaming to specie
+ using the Configure feature (contextual menu of the model) with the vle.discrete-time.generic/StepDates
+ edit the experimetal condition port:
  - Steps (a set of map)
  - Each map defines a step (Threshold = a string (month/day), Slope = a double, Value = a double
+ link the out observable port to the view
+ run and visualize


Three steps are necessary
+ 01/01 = 3
+ 01/15 = 0
+ 10/15 = 1
+ 11/01 = 2
+ 12/01 = 3