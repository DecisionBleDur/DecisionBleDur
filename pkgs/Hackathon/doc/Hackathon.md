# Hackathon Notes

warning: the CMakeList.txt file has been modified in order to manage .md documentation files.

The following line has been added:
install(DIRECTORY . DESTINATION doc FILES_MATCHING PATTERN "*.md")


Here you will find step by step elements to realize the Hackathon.

The steps are  materialized by simulators aka vpz files.

Each simulator has a <simulatorName>.md documentation file explaning how it has been build.

## step 0

* creating a new projet Hackathon.

## step 1 : sticsBouchon.vpz

A simple simulator providing a dataset READER in order to feed the overall simulator we are going to build.


## step 2 : portanceSol.vpz

## step 3 : varietes.vpz

## step 4 : semis.vpz
