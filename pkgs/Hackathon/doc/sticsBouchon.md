# sticsBouchon.vpz

this note explane how to build the simulator.

+ storing the **mod_swheat.csv** comming from the material folder, insidethe data folder of the package.
+ creating a new vpz file
+ renaming it to sticsBouchon.vpz
+ adding an atomicModel
+ renaming to sticsBouchon
+ using the dataConfigure feature (contextual menu of the model) with the record_meteo/generic_with_header_complete
+ edit the experimetal condition port:
  - duration : 365
  - PkgName : Hackathon
  - meteo_file : mod_swheat.csv
  - year_column : ian
  - month_column : mo
  - day_column : jo
  - column_separator: ;
+ add a parameter port to the simulation_engine condition
  - begin_date : 1994-10-17
+ connect the condition to the model
+ replace the output ports by :
  - precip
  - inn
  - azomes
  - RsurRU
  - QNplante
  - swfac
  - somupvtsem
+ replace the observable ports by
  - precip
  - inn
  - azomes
  - RsurRU
  - QNplante
  - swfac
  - somupvtsem
+ connect to the view
+ configure the view with header on top
+ run and visualize
