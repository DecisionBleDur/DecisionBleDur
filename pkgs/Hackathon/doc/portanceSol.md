# portanceSol.vpz

![portanceSol diagram](figures/portanceSol_diagram.png)

this note explane how to build the simulator.

+ copy the sticsBouchon.vpz simulator
+ renaming it to portanceSol.vpz
+ adding an atomicModel
+ renaming to portanceSol
+ using the Configure feature (contextual menu of the model) with the vle.discrete-time.generic/MovinSum
+ edit the experimetal condition port:
  - n : 7
+ link the precip sticsBouchon.precip port to the portanceSol.a
+ connect to the view:
  - MovingSum_a
+ run and visualize


Input ports can be renamed with a convention like this:

+ input : precip
+ output : MovingSum_precip

By the way the begin date parameter has been changed to test.